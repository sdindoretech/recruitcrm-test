<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Validator;

class ValidateMiddleware
{
    /**
     * Run the request filter.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {

        if( true == $this->validateToken( $request ) ) {

            if( substr( $request->url(), 0, 5 ) != "https" ) {
                return response()->json( 'Request not allowed', 400 );
            }

            return $next( $request );
        } else {
            return response()->json('Token authentication failed.', 403 );
        }

    }

    public function validateToken( $request ) {

        if( !$request->bearerToken() ) {
            return false;
        }

        try{
            $objUser = \App\User::where( 'auth_token', $request->bearerToken() )->firstOrFail();
            return true;
        } catch( \Throwable $e ) {
           return false;
        }

    }

}