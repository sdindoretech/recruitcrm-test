<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Validator;

class CandidateMiddleware
{
    /**
     * Run the request filter.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $validator = Validator::make($request->all(), 
              [ 
                'first_name'        => 'required|max:40',
                'last_name'         => 'max:40',
                'email'             => 'unique:candidates|max:100',
                'contact_number'    => 'unique:candidates|max:100',
                'gender'            => 'in:1,2',
                'specialization'    => 'max:200',
                'work_x_year'       => 'lte:30',
                'address'           => 'max:500',
                'candidate_dob'     => 'date',
             ]);   

        if ( $validator->fails() ) {          
            return response()->json( ['error'=>$validator->errors()], 422 ); 
        }
         
        return $next( $request );

    }

}