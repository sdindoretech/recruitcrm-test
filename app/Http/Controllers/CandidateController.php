<?php

namespace App\Http\Controllers;

use App\Candidate;
use Illuminate\Http\Request;

class CandidateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index( Request $request )
    {
        $intPerPage = 10;
        if ($request->has('per_page')) {
            $intPerPage = $request->input('per_page');
        }

        $arrmixCandidates = Candidate::select( 'first_name', 'last_name', 'email', 'contact_number', Candidate::raw( '( CASE WHEN gender = 1 THEN "Male" ELSE "Female" END ) AS gender' ), 'specialization', 'work_x_year', 'candidate_dob', 'created_on', 'updated_on', 'address', 'resume' )->paginate( $intPerPage );

        $response = Response([
            'status'=> 'success',
            'statuscode' => 200,
            'data' => $arrmixCandidates
        ]);

        return $response;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $uploadedFile       = $this->uploadFile( $request );
        $candidate          = $this->mapInputObject( $request );
        $candidate->resume  = $uploadedFile;

        try {
    
            $candidate->save();

            return Response([
                'status' => 'created',
                'statuscode' => 201,
                'data' => $candidate
            ], 201); 
            
        } catch (\Throwable $e) {

            return Response([
                'status' => 'Internal Server Error',
                'statuscode' => 500,
                'data' => $e->getMessage()
            ], 500); 
            
        }
      
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Candidate  $candidate
     * @return \Illuminate\Http\Response
     */
    public function show(Candidate $candidate, $id )
    {

        $arrmixCandidate = Candidate::select( 'first_name', 'last_name', 'email', 'contact_number', Candidate::raw( '( CASE WHEN gender = 1 THEN "Male" ELSE "Female" END ) AS gender' ), 'specialization', 'work_x_year', 'candidate_dob', 'created_on', 'updated_on', 'address', 'resume' )->where( 'id', $id )->get();
        $response = Response([
            'status'=> 'success',
            'statuscode' => 200,
            'data' => $arrmixCandidate
        ]);

        return $response;
    }

    public function search( Request $request )
    {
        $intPerPage = 10;
        $arrmixWhereClause = [];

        if ($request->has('per_page')) {
            $intPerPage = $request->input('per_page');
        }

        if ($request->has('first_name')) {
            $arrmixWhereClause['first_name'] = $request->input('first_name');
        }

        if ($request->has('last_name')) {
            $arrmixWhereClause['last_name'] = $request->input('last_name');
        }

        if ($request->has('email')) {
            $arrmixWhereClause['email'] = $request->input('email');
        }

        $arrmixCandidates = Candidate::select( 'first_name', 'last_name', 'email', 'contact_number', Candidate::raw( '( CASE WHEN gender = 1 THEN "Male" ELSE "Female" END ) AS gender' ), 'specialization', 'work_x_year', 'candidate_dob', 'created_on', 'updated_on', 'address', 'resume' )->where( $arrmixWhereClause )->paginate( $intPerPage );

        $response = Response([
            'status'=> 'success',
            'statuscode' => 200,
            'data' => $arrmixCandidates
        ]);

        return $response;
    }


    public function uploadFile( Request $request ) {

        $uploadedFile = '';

        if($request->hasFile('resume') ) {
            $original_filename = $request->file( 'resume' )->getClientOriginalName();
            $original_filename_arr = explode( '.', $original_filename );
            $file_ext = end( $original_filename_arr );
            $destination_path = './public/uploads/';
            $image = date( 'YmdHis' ) . '.' . $file_ext;

            if ($request->file( 'resume' )->move( $destination_path, $image ) ) {
                $uploadedFile = $destination_path . $image;
            }

        }

        return $uploadedFile;

    }

    public function mapInputObject( Request $request ) {

        $candidate = new Candidate();
        $candidate->first_name = $request->first_name;
        $candidate->last_name = $request->last_name;
        $candidate->email = $request->email;
        $candidate->contact_number = $request->contact_number;
        $candidate->gender = $request->gender;
        $candidate->specialization = $request->specialization;
        $candidate->work_x_year = $request->work_x_year;
        $candidate->candidate_dob = strtotime( $request->candidate_dob );
        $candidate->address = $request->address;
        $candidate->resume = NULL;

        return $candidate;
    }
}