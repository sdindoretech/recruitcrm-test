<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Candidate extends Model
{
    //
    const CREATED_AT = 'created_on';
    const UPDATED_AT = 'updated_on';
    // public $timestamps = false;

    public function getDateFormat()
    {
        return 'U';
    }

}
