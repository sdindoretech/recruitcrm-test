<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            Schema::create('users', function (Blueprint $table) {
                $table->id();
                $table->string( 'full_name', 40 );
                $table->string( 'user_name', 10 )->unique();
                $table->string( 'password', 100 );
                $table->string( 'auth_token', 60 )->unique();
                $table->integer( 'created_on' );
                $table->integer( 'updated_on' );
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
