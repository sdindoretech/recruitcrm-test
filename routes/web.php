<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

// $router->get('/', function () use ($router) {
//     return $router->app->version();
// });

//https://stackoverflow.com/questions/34339517/lumen-laravel-eloquent-php-artisan-makemodel-not-defined

$router->group(['prefix'=>'v1/candidates', 'middleware'=>'validate'], function() use( $router ){

    $router->get( '/', 'CandidateController@index' );
    $router->get( '/{id}', 'CandidateController@show' );
    // $router->get( '/search', 'CandidateController@search' );
    
    $router->post( '/', [ 'middleware' => 'candidate', 'uses' => 'CandidateController@store' ]);
});